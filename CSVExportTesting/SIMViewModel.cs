﻿using System;

namespace CSVExportTesting
{
    using CSVExport;
    public class SIMViewModel : ICSVExportable
    {
        public int SIMID { get; set; }
        public string SIMNumber { get; set; }
        public string GPRSPlanName { get; set; }
        public string SMSPlanName { get; set; }
        public string VOICEPlanName { get; set; }

        [CSVMember("Usage Date")]
        public string UsageDateString
        {
            get { return _usageDate.ToString("MM/dd/yyyy"); }
        }
        private DateTime _usageDate;

        private ICSVValuesParser _valuesParser;
        private ICSVColumnsParser _columnsParser;
        public SIMViewModel(ICSVValuesParser vParser, ICSVColumnsParser cParser)
        {
            this._valuesParser = vParser;
            this._columnsParser = cParser;
        }
        
        public SIMViewModel() : this(new CSVPropertyValuesParser(), new CSVPropertyColumnsParser())
        {

        }
        public SIMViewModel(SIMThresholdsForSIM sim) : this()
        {
            this.SIMID = sim.SIMID.Value;
            this.SIMNumber = sim.SIMNumber;
            this.SMSPlanName = sim.SMSPlanName;
            this.VOICEPlanName = sim.VOICEPlanName;
            this.GPRSPlanName = sim.GPRSPlanName;
            this._usageDate = sim.usageDate.Value;
        }
        public string ToCSV()
        {
            return _valuesParser.ParseObject(this);
        }

        public string GetCSVColumns()
        {
            return _columnsParser.GetColumns(this.GetType());
        }
    }
}
