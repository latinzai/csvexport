﻿using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using CSVExport;

namespace CSVExportTesting
{
    [TestClass]
    public class CSVExportTest
    {
        private const string file = @"C:\Users\jdiaz\Desktop\sampleviewmodel.csv";
        private IList<SampleViewModel> list;

        public CSVExportTest()
        {
            this.list = new List<SampleViewModel>()
            {
                new SampleViewModel()
                {
                    Age = 21,
                    BirthDateString = "12/12/1966",
                    Name = "Jorge Armando"
                },
                new SampleViewModel()
                {
                    Age = 25,
                    BirthDateString = "12/12/1946",
                    Name = "Paul Molina"
                },
                new SampleViewModel()
                {
                    Age = 19,
                    BirthDateString = "12/12/1946",
                    Name = "Octavio Suero"
                },
                new SampleViewModel()
                {
                    Age = 46,
                    BirthDateString = "12/12/1998",
                    Name = "Jorge Diaz"
                },
            };

            CleanUp();
        }

        [TestMethod]
        public void Should_Create_File()
        {
            CleanUp();
            File.WriteAllText(file, list.ToCSV());
            Assert.IsTrue(File.Exists(file));
        }
        [TestMethod]
        public void File_Has_Contents()
        {
            var contents = File.ReadAllText(file);
            Assert.IsFalse(string.IsNullOrEmpty(contents));
        }

        [TestMethod]
        public void File_Exists_And_Has_Contents()
        {
            Should_Create_File();
            File_Has_Contents();
        }
        [TestMethod]
        public void Are_Columns_Sorted()
        {
            var columns = File.ReadAllLines(file)[0];

        }
        private void CleanUp()
        {
            try
            {
                File.Delete(file);
            }
            catch
            {
               // EMPTY;
            }
        }
    }
}
