﻿using CSVExport;
using System;

namespace CSVExportTesting
{
    public class SIMThresholdsForSIM
    {
        #region "members"

        #endregion

        #region "Public Properties"
        [CSVMember(ColumnName = "SIM ID")]
        public int? SIMID { get; set; }
        [CSVMember(ColumnName = "Usage Date")]
        public DateTime? usageDate { get; set; }
        [CSVMember(ColumnName = "SIM Number")]
        public string SIMNumber { get; set; }
        [CSVMember]
        public string MSISDN { get; set; }
        [CSVMember(ColumnName = "GPRS Total")]
        public long? GPRSTotal { get; set; }
        public int? GPRSThreshold { get; set; }
        public long? SMSTotal { get; set; }
        public int? SMSThreshold { get; set; }
        public int? VOICETotal { get; set; }
        public int? DailyVOICEThreshold { get; set; }
        public int? monthlyGPRSThreshold { get; set; }
        public int? monthlySMSThreshold { get; set; }
        public int? MonthlyVOICEThreshold { get; set; }
        public long? monthGPRSTotal { get; set; }
        public long? monthSMSTotal { get; set; }
        public long? MonthVOICETotal { get; set; }
        public long? monthGPRSOverage { get; set; }
        public long? monthSMSOverage { get; set; }
        public long? MonthVOICEOverage { get; set; }
        public DateTime? billingCDRStartDate { get; set; }
        public int? GPRSSIMPlanClassTypeId { get; set; }
        public int? SMSSIMPlanClassTypeId { get; set; }
        public string GPRSPlanName { get; set; }
        public string SMSPlanName { get; set; }
        public string VOICEPlanName { get; set; }
        public string CostCenterFullName { get; set; }
        public string CostCenterDisplayName { get; set; }
        #endregion

        #region "public methods"

        public SIMThresholdsForSIM(int? pSIMID, DateTime? pUsageDate, string pMSISDN, string pSIMNumber,
            long? pGPRSTotal, int? pGPRSThreshold,
            int? pSMSTotal, int? pSMSThreshold,
            int? pVOICETotal, int? pVOICEThreshold,
            int? pMonthlyGPRSThreshold, int? pMonthlySMSThreshold, int? pMonthlyVOICEThreshold,
            long? pMonthGPRSTotal, long? pMonthSMSTotal, long? pMonthVOICETotal,
            long? pMonthGPRSOverage, long? pMonthSMSOverage, long? pMonthVOICEOverage,
            int? pGPRSSIMPlanClassTypeId, int? pSMSSIMPlanClassTypeId,
            string pGPRSPlanName, string pSMSPlanName, string pVOICEPlanName, DateTime?
            pBillingCDRStartDate, string costCenterFullName,
            string costCenterDisplayName)
        {
            SIMID = pSIMID;
            usageDate = pUsageDate;
            MSISDN = pMSISDN;
            SIMNumber = pSIMNumber;
            GPRSTotal = pGPRSTotal;
            GPRSThreshold = pGPRSThreshold;
            SMSTotal = pSMSTotal;
            SMSThreshold = pSMSThreshold;
            VOICETotal = pVOICETotal;
            DailyVOICEThreshold = pVOICEThreshold;
            monthlyGPRSThreshold = pMonthlyGPRSThreshold;
            monthlySMSThreshold = pMonthlySMSThreshold;
            MonthlyVOICEThreshold = pMonthlyVOICEThreshold;
            monthGPRSTotal = pMonthGPRSTotal;
            monthSMSTotal = pMonthSMSTotal;
            MonthVOICETotal = pMonthVOICETotal;
            monthGPRSOverage = pMonthGPRSOverage;
            monthSMSOverage = pMonthSMSOverage;
            MonthVOICEOverage = pMonthVOICEOverage;
            SMSSIMPlanClassTypeId = pSMSSIMPlanClassTypeId;
            GPRSSIMPlanClassTypeId = pGPRSSIMPlanClassTypeId;
            GPRSPlanName = pGPRSPlanName;
            SMSPlanName = pSMSPlanName;
            VOICEPlanName = pVOICEPlanName;
            billingCDRStartDate = pBillingCDRStartDate;
            CostCenterDisplayName = costCenterDisplayName;
            CostCenterFullName = costCenterFullName;
        }

        #endregion //public methods
    }
}
