﻿namespace CSVExportTesting
{
    using CSVExport;
    public class SampleViewModel : CSVViewModelBase
    {
        [CSVMember("Full Name", 1)]
        public string Name { get; set; }
        public int Age { get; set; }
        [CSVMember("Birth Date", 0)] 
        public string BirthDateString { get; set; }

        //public override string GetCSVColumns()
        //{
        //    return "Column1, Column2, Column3";
        //}
    }
}
