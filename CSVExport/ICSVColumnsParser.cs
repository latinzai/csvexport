﻿using System;

namespace CSVExport
{
    public interface ICSVColumnsParser
    {
        string GetColumns(Type type);
    }
}
