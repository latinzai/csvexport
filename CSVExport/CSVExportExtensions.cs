using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace CSVExport
{
    public static class CSVExportExtensions
    {
        public static string ToCSV<T>(this IList<T> list) where T : ICSVExportable
        {
           
            var builder = new StringBuilder();

            var columns = GetColumnsFromList(list);

            if (!string.IsNullOrEmpty(columns))
                builder.AppendLine(columns);

            foreach (var item in list)
                builder.AppendLine(item.ToCSV());

            return builder.ToString();
        }

        private static string GetColumnsFromList<T>(IList<T> list) where T : ICSVExportable
        {
            var item = list.FirstOrDefault();
            return item.GetCSVColumns();
        }

        public static string ToCSV(this DataTable table)
        {
            var builder = new StringBuilder();
            var columns = table.Columns.Cast<DataColumn>().Select(c => c.ColumnName);
            builder.AppendLine(string.Join(",", columns));

            table.Rows.Cast<DataRow>().ToList().ForEach((r) =>
            {

                string[] data = r.ItemArray.Select(field => field.ToString()).
                                    ToArray();
                builder.AppendLine(string.Join(",", data));

            });

            return builder.ToString();
        }

        public static string ToCSV(this DataSet dataset)
        {
            try
            {
                var table = dataset.Tables[0];
                return table.ToCSV();
            }
            catch
            {
                return "DataSet contains no tables.";
            }
        }
    }
}
