﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSVExport
{
    public interface ICSVExportable
    {
        string ToCSV();
        string GetCSVColumns();
    }
}
