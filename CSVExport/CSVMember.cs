﻿using System;

namespace CSVExport
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class CSVMember : Attribute
    {
        public string ColumnName { get; set; }
        public int ColumnOrder { get; set; }

        public CSVMember()
        {

        }
        public CSVMember(string columnName)
        {
            this.ColumnName = columnName;
        }

        public CSVMember(string columnName, int columnOrder)
        {
            this.ColumnName = columnName;
            this.ColumnOrder = columnOrder;
        }
        public CSVMember(int columnOrder)
        {
            this.ColumnOrder = columnOrder;
        }
    }
}
