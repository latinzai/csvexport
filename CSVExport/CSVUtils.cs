﻿using System;
using System.Reflection;

namespace CSVExport
{
    public class CSVUtils
    {
        public static string GetCSVMemberColumnName(Type type, string propertyName)
        {
            var csvAttribute = (CSVMember)type.GetProperty(propertyName).GetCustomAttribute(typeof(CSVMember), false);

            if (csvAttribute != null && !string.IsNullOrEmpty(csvAttribute.ColumnName))
            {
                return csvAttribute.ColumnName;
            }

            return propertyName;
        }

        public static int GetCSVMemberColumnOrder(Type type, string propertyName)
        {
            var csvAttribute = (CSVMember)type.GetProperty(propertyName).GetCustomAttribute(typeof(CSVMember), false);

            if (csvAttribute != null)
            {
                return csvAttribute.ColumnOrder;
            }

            return short.MaxValue;
        }
    }
}
