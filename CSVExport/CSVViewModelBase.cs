﻿
namespace CSVExport
{
    public class CSVViewModelBase : ICSVExportable
    {
        private ICSVValuesParser _valuesParser;
        private ICSVColumnsParser _columnsParser;

        public CSVViewModelBase(ICSVValuesParser valuesParser, ICSVColumnsParser columnsParser)
        {
            this._valuesParser = valuesParser;
            this._columnsParser = columnsParser;
        }

        public CSVViewModelBase() : this(new CSVPropertyValuesParser(), new CSVPropertyColumnsParser())
        {

        }

        public virtual string GetCSVColumns()
        {
            return _columnsParser.GetColumns(GetType());
        }

        public virtual string ToCSV()
        {
            return _valuesParser.ParseObject(this);
        }
    }
}
