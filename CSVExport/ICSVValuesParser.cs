﻿
namespace CSVExport
{
    public interface ICSVValuesParser
    {
        string ParseObject(object objectToParse);
    }
}
