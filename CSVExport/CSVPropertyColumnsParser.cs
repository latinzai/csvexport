﻿using System;
using System.Linq;
using System.Reflection;

namespace CSVExport
{
    public class CSVPropertyColumnsParser : ICSVColumnsParser
    {
        public string GetColumns(Type type)
        {
            var columns = type.GetProperties(BindingFlags.Public | BindingFlags.Instance)
                                .OrderBy(p => CSVUtils.GetCSVMemberColumnOrder(type, p.Name))
                                .Select(p => CSVUtils.GetCSVMemberColumnName(type, p.Name))
                                .ToArray();

            return string.Join(",", columns);
        }
    }
}
