﻿using System;
using System.Linq;
using System.Reflection;

namespace CSVExport
{
    public class CSVPropertyValuesParser : ICSVValuesParser
    {
        private string GetValuesFromObject(object obj)
        {
            Type itemType = obj.GetType();

            var values = itemType.GetProperties(BindingFlags.Public | BindingFlags.Instance)
                                .OrderBy(p => CSVUtils.GetCSVMemberColumnOrder(itemType, p.Name))
                                .Select(p => p.GetValue(obj, null).ToString())
                                .ToArray();

            return string.Join(",", values);
        }

        public string ParseObject(object objectToParse)
        {
            return GetValuesFromObject(objectToParse);
        }
    }
}
